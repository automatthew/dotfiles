" Vim syntax file
" Language:    Pez
" Maintainer:  Pete Elmore <pete.elmore@gmail.com>
" Filenames:   *.pez
" URL:	       Not yet
"
" Loosely based on Brüssow's syntax/forth.vim, which is included with vim, and
" from which I copied several runes.  It is fairly incomplete, and misses a lot.
" (Currently, it only describes definitions and comments.)

" For version 5.x: Clear all syntax items
" For version 6.x: Quit when a syntax file was already loaded
if version < 600
    syntax clear
elseif exists("b:current_syntax")
    finish
endif

syn sync ccomment
syn sync maxlines=200

syn keyword pezTodo contained TODO FIXME XXX TODO: FIXME: XXX:

" Characters allowed in keywords
if version >= 600
    setlocal iskeyword=!,@,33-35,%,$,38-64,A-Z,91-96,a-z,123-126,128-255
else
    set iskeyword=!,@,33-35,%,$,38-64,A-Z,91-96,a-z,123-126,128-255
endif

syn match pezColonDef '\<:\s\+[^ \t]\+\>'
syn keyword pezEndOfColonDef ;
syn match pezFFIDef '\<ffi:m\?\s*[^ \t]\+\>'
syn match pezEndOfFFIDef '[$ \t]'

syn match pezComment '#\s.*$' contains=pezTodo
syn region pezComment start='\\S\s' end='.*' contains=pezTodo
syn match pezComment '(\s[^)]*)' contains=pezTodo
syn region pezComment start='\(^\|\s\)\zs(\(\s\|$\)' skip='\\)' end=')' contains=pezTodo

if version >= 508 || !exists("did_pez_syn_inits")
	if version < 508
		let did_pez_syn_inits = 1
		command -nargs=+ HiLink hi link <args>
	else
		command -nargs=+ HiLink hi def link <args>
	endif

	HiLink pezTodo Todo
	HiLink pezComment Comment
	HiLink pezColonDef Define
	HiLink pezEndOfColonDef Define
	HiLink pezFFIDef Define

	delcommand HiLink
endif

let b:current_syntax = "pez"
